	public class ArchivoEquipos{
private String placas[]={"349542","349559","355851","370384","355936 ","355861","355925","370459"};//a. Un atributo privado de tipo arreglo de String llamado placas.
private String marcas[]={"Samsung","Samsung","OLYMPUS","Manfrotto","Canon","Canon","Honeywell 1","Printrbot"};//b. Un atributo privado de tipo arreglo de String llamado marcas.
private String modelos[]={" GT-P5100"," GT-P5100","WS-802"," 755XB","T5i","T3","1400G:1D/PDF417/","Printrbot Metal Simple ASM"};//c. Un atributo privado de tipo arreglo de String llamado modelos.
private String tipos[]={"Tablet","Tablet","Grabadora","Trípode","Cámara fotográfica","Cámara fotográfica","Lectora de codigos de barra","Impresora 3D"};//d. Un atributo privado de tipo arreglo de String llamado tipos.
private String acesorios[]={"Cargador y batería", "Cargador y batería","Cargador, estuche y batería","Cabeza, trípode, estuche","OBJETIVO CANON 75-300MM, Estuche, Cargador, Batería original, Batería genérica, SD Card 64Gb","OBJETIVO CANON 75-300MM, Estuche Cargador, Batería original, Batería genérica, SD Card 64Gb","Sin accesorios","Filamentos color azul, verde y gris"};//e. Un atributo privado de tipo arreglo de String llamado accesorios.
private boolean estados[]={true,false,true,true,true,false,true,true};//f. Un atributo privado de tipo arreglo de boolean llamado estados.

public String getPlaca(int indice){//h. Un método llamado getPlaca que recibe un índice y retorna la placa de esa posición
	return placas[indice];
}
public String getMarca(int indice){//i. Un método llamado getMarca que recibe un índice y retorna la marca de esa posición.
	return marcas[indice];
}
public String getModelo(int indice){//j. Un método llamado getModelo que recibe un índice y retorna la modelo de esa posición.
	return modelos[indice];
}
public String getAcesorios(int indice){//k. Un método llamado getAccesorios que recibe un índice y retorna los accesorios de esa posición.
	return acesorios[indice];
}
public String getTipo(int indice){//l. Un método llamado getTipo que recibe un índice y retorna la marca de esa posición
	return tipos[indice];
}
public boolean getEstado(int indice){//m. Un método llamado getEstado que recibe un índice y retorna el estado de esa posición.
	return estados[indice];
}
public int length(){//n. Un método llamado length que retorna el tamaño de los registros almacenados. Es decir la cantidad de equipos almacenados.
		return placas.length;
		}



}
