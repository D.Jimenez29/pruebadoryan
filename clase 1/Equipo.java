 public class Equipo	{
private String placa;
private String marca;
private String modelo;
private String tipo;
private String acesorios;
private boolean estado;

public Equipo(String placa, String marca, String modelo, String tipo, String acesorios, boolean estado){
setPlaca(placa);
setMarca(marca);
setModelo(modelo);
setTipo(tipo);
setAcesorios(acesorios);
setEstado(estado);
}
public Equipo(){
}
public void setPlaca(String placa){
this.	placa=placa;	
}
public String getPlaca(){
return placa;
}
public void setMarca(String marca){
this.	marca=marca;	
}
public String getMarca(){
return marca;
}
public void setModelo(String modelo){
this.	modelo=modelo;	
}
public String getModelo(){
return modelo;
}
public void setTipo(String tipo){
this.	tipo=tipo;
}
public String getTipo(){
return tipo;
}
public void setAcesorios(String acesorios){
this.	acesorios=acesorios;	
}
public String getAcesorios(){
return acesorios;
}
public void setEstado(boolean estado){
	this.estado=estado;
	}
	public boolean getEstado(){
		return estado;
		}
public String toString(){
return "La placa es "+this.getPlaca()+"\n La marca es: "+this.getMarca()+"\n  El modelo es: "+this.getModelo()+"\n Su tipo es:  "+this.getModelo()+" \n Y sus acesorios son: "+this.getAcesorios()+"\nEstado "+this.getEstado();
}
}
